﻿using EveMag.Context;
using EveMag.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EveMag.Controllers
{
    public class EventDetailsController : Controller
    {
        EventDetailsContext eContext = new EventDetailsContext();
        // GET: EventDetails
        public ActionResult Index()
        {
            return View(eContext.GetEventDetails().ToList());
        }

        // GET: EventDetails/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: EventDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EventDetails/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                EventDetails eve = new EventDetails();
                eve.EventDetailsId = eContext.GetEventDetails().Count + 1;
                eve.Adults = Convert.ToInt32(collection[1]);
                eve.Kids = Convert.ToInt32(collection[2]);
                eve.Vegetarian = Convert.ToInt32(collection[3]);
                eve.Non_Vegetarian = Convert.ToInt32(collection[4]);
                eve.Transport = true;
                eve.Alerts = false;
                eContext.AddEventDetails(eve);
                // TODO: Add insert logic here
             
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EventDetails/Edit/5
        public ActionResult Edit(int id)
        {
            var eve = eContext.Get(id);
            return View("Edit", eve);
            
        }

        // POST: EventDetails/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EventDetails eve)
        {
            try
            {
                // TODO: Add update logic here
                eContext.UpdateEventDetails(eve);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
                throw ex;
            }
        }

        // GET: EventDetails/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EventDetails/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
