﻿using EveMag.Models;
using EveMag.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EveMag.Controllers
{
   
    public class FeedbackController : Controller
    {
        FeedbackContext fContext = new FeedbackContext();
        // GET: Feedback
        public ActionResult Index()
        {
            return View(fContext.GetFeedbacks().ToList());
        }

        // GET: Feedback/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Feedback/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Feedback/Create
        [HttpPost]
        
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                Feedback fb = new Feedback();
                fb.FId = fContext.GetFeedbacks().Count + 1;
                fb.Rating = Convert.ToInt32(collection[1]);
                fb.FeedbackDesc = Convert.ToString(collection[2]);
                fb.EmpUsername = Convert.ToString(collection[3]);
                fb.EventId = Convert.ToInt32(collection[4]);
                fContext.AddFeedback(fb);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Feedback/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Feedback/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Feedback/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Feedback/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
