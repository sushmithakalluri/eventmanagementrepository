﻿using EveMag.Models;
using EveMag.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EveMag.Controllers
{
    public class EventController : Controller
    {
        EventContext eContext = new EventContext();
        // GET: Event
        public ActionResult Index()
        {
            return View(eContext.GetEvents().ToList());
            
        }

        // GET: Event/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Event/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Event/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                Event eve = new Event();
               // eve.EventId = eContext.GetEvents().Count + 1;
                eve.Name = collection[1];
                eve.Date = Convert.ToDateTime(collection[2]);
                eve.Venue = Convert.ToString(collection[3]);
                eve.Description = Convert.ToString(collection[4]);
                eve.SeekFeedback = true;// Convert.ToBoolean(collection[5]);
                eve.VolunteerReq = false;
                eContext.AddEvent(eve);
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Event/Edit/5
        public ActionResult Edit(int id)
        {
            var eve = eContext.Get(id);
            return View("Edit", eve);
        }

        // POST: Event/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Event eve)
        {
            try
            {
                // TODO: Add update logic here
                eContext.UpdateEvent(eve);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Event/Delete/5
        public ActionResult Delete(int id)
        {
            var eve = eContext.Get(id);
            return View("Delete", eve);
        }

        // POST: Event/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                eContext.DeleteEvent(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
