﻿using EveMag.Models;
using EveMag.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EveMag.Controllers
{
    public class DeactivateController : Controller
    {
        DeactivateContext dc = new DeactivateContext();
        // GET: Deactivate
        public ActionResult Index()
        {
            return View(dc.GetEmployees().ToList());
        }

        // GET: Deactivate/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Deactivate/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Deactivate/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            { 

                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Deactivate/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Deactivate/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Deactivate/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Deactivate/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
