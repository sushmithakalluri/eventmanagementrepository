﻿using EveMag.Models;
using EveMag.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EveMag.Controllers
{
    public class LoginController : Controller
    {
        LoginContext lcontext = new LoginContext();
        // GET: Login
        public ActionResult Index()
        {
            //return View("~/Views/Event/Index.cshtml");
            return View();
        }
        // GET: Login/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Login/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                Login objLogin = new Login();
                objLogin.Username = Convert.ToString(collection["Username"]);
                objLogin.Password = Convert.ToString(collection["Password"]);
                //objLogin.Password = Utility.EncryptPassword(objLogin.Password);
                int retVal = lcontext.ValidateUser(objLogin);
                if (retVal == 1)
                {
                    if(objLogin.Username == "admin")
                    {
                        return RedirectToAction("Index");//AdminHomePage
                    }
                    else
                    {
                        return RedirectToAction("Details");//EmployeeHomePage
                    }
                   
                }
                else
                {
                    ViewBag.message = "Please enter valid details ";
                }
                return View();

            }
            catch
            {
                return View();
            }
        }
        // GET: Event/Details/5
        public ActionResult Details()
        {
            return View();
        }
        // GET: Event/Edit/5
        public ActionResult Edit()
        {
           
            return View();
        }
    }
}
