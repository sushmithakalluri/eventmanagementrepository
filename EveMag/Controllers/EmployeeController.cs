﻿using EveMag.Models;
using EveMag.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace EveMag.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeContext empContext = new EmployeeContext();
        // GET: Employee
        public ActionResult Index()
        {
            return View(empContext.GetEmployees().ToList());
        }

        // GET: Employee/Details/5
        public int ValidateUserName(string username)
        {
            EmployeeContext emp = new EmployeeContext();
            return emp.VerifyUserName(username);

        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create(Employee emp)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    emp.Password = Utility.EncryptPassword(emp.Password);
                    empContext.AddEmployee(emp);
                    return RedirectToAction("Index");
                }
                catch
                {
                    return View();
                }
            }
            return View();
        }



        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {

            var emp = empContext.Get(id);
            return View("Edit", emp);
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Employee emp)
        {
            try
            {

                emp.Password = Utility.EncryptPassword(emp.Password);
                empContext.UpdateEmployee(emp);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

       
    }
}
