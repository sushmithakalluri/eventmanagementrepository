﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EveMag.Startup))]
namespace EveMag
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
