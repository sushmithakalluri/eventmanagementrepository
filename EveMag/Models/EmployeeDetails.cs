﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EveMag.Models
{
    /// <summary>
    /// Employee Class
    /// </summary>
    public class Employee
    {
        //int id;
        //string firstName;
        //string lastName;
        //string userName;
        //long phoneNumber;
        //string emailId;
        //string password;
        //        int eventDetailsId;

        public Employee()
        {

        }
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public long PhoneNumber { get; set; }

        public string EmailId { get; set; }

        public string Password { get; set; }
    }
}