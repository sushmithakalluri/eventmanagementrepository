﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EveMag.Models
{
    public class EventDetails 
    {


        public int EventDetailsId { get; set; }

        public int Adults { get; set; }

        public int Kids { get; set; }

        public int Vegetarian { get; set; }

        public int Non_Vegetarian { get; set; }

        public bool Transport { get; set; }

        public bool Alerts { get; set; }




    }
}
