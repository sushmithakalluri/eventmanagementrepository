﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EveMag.Models
{
    
    public class Feedback
    {
        [Key]
        public int FId { get; set; }

        public int Rating { get; set; }

        public string FeedbackDesc { get; set; }

        public string EmpUsername { get; set; }

        public int EventId { get; set; }
    }
}