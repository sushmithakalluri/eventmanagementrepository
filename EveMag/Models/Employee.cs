﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EveMag.Models
{
    public class Employee
    {
        [Key]
        [Required]
        public int EmpId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        [Required]
        public string UserName { get; set; }

        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        [Required]
        public string EmailId { get; set; }
        [Required]
        
        //[Range(minimum: 4, maximum: 10, ErrorMessage = "Password string length should be between 4 and 10")]
       
        public bool IsActive { get; set; }
    }
}