﻿using EveMag.Repository;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace EveMag.Models
{
    public class Login
    {
        [Key]
        public string Username { get; set; }

        public string Password { get; set; }

        public int EmpId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailId { get; set; }

        public string PhoneNumber { get; set; }

        public bool IsActive { get; set; }
        
    }
    
  }
