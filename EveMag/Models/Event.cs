﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EveMag.Models
{
    public class Event
    {
        public int EventId { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public string Venue { get; set; }

        public string Description { get; set; }

        public bool SeekFeedback { get; set; }

        public bool VolunteerReq { get; set; }

        public bool IsPublished { get; set; }
    }
}