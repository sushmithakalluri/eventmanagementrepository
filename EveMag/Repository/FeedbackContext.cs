﻿using EveMag.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace EveMag.Repository
{
    public class FeedbackContext : BaseDataContext
    {

        public int AddFeedback(Feedback fb)
        {
            int retVal = -1;
            string storedProcName = "AddFeedback";
            MySqlConnection connection = null;
            MySqlCommand command = null;
            try
            {
                connection = GetConnection();
                connection.Open();

                command = new MySqlCommand(storedProcName, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("FId", fb.FId);
                command.Parameters.AddWithValue("rating", fb.Rating);
                command.Parameters.AddWithValue("feedbackDesc", fb.FeedbackDesc);
                command.Parameters.AddWithValue("empUsername", fb.EmpUsername);
                command.Parameters.AddWithValue("eventId", fb.EventId);


                retVal = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Log the exception
                ExceptionLogger.SendErrorToText(ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (connection != null && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return retVal;
        }

        public List<Feedback> GetFeedbacks()
        {
            MySqlCommand cmd = new MySqlCommand("GetFeedbacks", GetConnection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection.Open();
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            List<Feedback> Feedbackslist = new List<Feedback>();
            while (dr.Read())
            {
                Feedback fb = new Feedback();
                fb.FId = Convert.ToInt32(dr[0]);
                fb.Rating = Convert.ToInt32(dr[1]);
                fb.FeedbackDesc = Convert.ToString(dr[2]);
                fb.EmpUsername = Convert.ToString(dr[3]);
                fb.EventId = Convert.ToInt32(dr[4]);

                Feedbackslist.Add(fb);
            }
            dr.Close();
            return Feedbackslist;
        }
    }
}
