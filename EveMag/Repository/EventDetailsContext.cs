﻿using EveMag.Models;
using EveMag.Repository;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace EveMag.Context
{
    public class EventDetailsContext : BaseDataContext
    {
        string connectionString = "";

        public int AddEventDetails(EventDetails eve)
        {
            int retVal = -1;
            string storedProcName = "AddEventDetails";
            MySqlConnection connection = null;
            MySqlCommand command = null;

            try
            {
                connection = GetConnection();
                connection.Open();

                command = new MySqlCommand(storedProcName, connection);


                command.CommandType = CommandType.Text;
                command.CommandText = "select max(eventdetailsId)+1 from eventdetails";
                object val = command.ExecuteScalar();
                eve.EventDetailsId = Convert.ToInt32(val);


                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = storedProcName;
                command.Parameters.AddWithValue("eventdetailsId", eve.EventDetailsId);
                command.Parameters.AddWithValue("adults", eve.Adults);
                command.Parameters.AddWithValue("kids", eve.Kids);
                command.Parameters.AddWithValue("vegetarian", eve.Vegetarian);
                command.Parameters.AddWithValue("non_vegetarian", eve.Non_Vegetarian);
                command.Parameters.AddWithValue("transport", eve.Transport);
                command.Parameters.AddWithValue("alerts", eve.Alerts);
                retVal = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Log the exception
                ExceptionLogger.SendErrorToText(ex);
                throw ex;
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (connection != null && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return retVal;
        }
        public EventDetails Get(int Id)
        {
            return GetEventDetails().Where(x => x.EventDetailsId == Id).First();
        }
        public List<EventDetails> GetEventDetails()
        {

            //connection();
            MySqlCommand cmd = new MySqlCommand("GetEventDetails", GetConnection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection.Open();
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            List<EventDetails> EventDetailslist = new List<EventDetails>();
            while (dr.Read())
            {
                EventDetails Eve = new EventDetails();
                Eve.EventDetailsId = Convert.ToInt32(dr[0]);
                Eve.Adults = Convert.ToInt32(dr[1]);
                Eve.Kids = Convert.ToInt32(dr[2]);
                Eve.Vegetarian = Convert.ToInt32(dr[3]);
                Eve.Non_Vegetarian = Convert.ToInt32(dr[4]);
                Eve.Transport = Convert.ToBoolean(dr[5]);
                Eve.Alerts = Convert.ToBoolean(dr[6]);

                EventDetailslist.Add(Eve);
            }
            dr.Close();
            return EventDetailslist;
        }
        public int UpdateEventDetails(EventDetails eve)
        {

            int retVal = -1;
            string storedProcName = "EditEventDetails";
            MySqlConnection connection = null;
            MySqlCommand command = null;

            try
            {
                connection = GetConnection();
                connection.Open();

                command = new MySqlCommand(storedProcName, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("edId", eve.EventDetailsId);
                command.Parameters.AddWithValue("adults", eve.Adults);
                command.Parameters.AddWithValue("kids", eve.Kids);
                command.Parameters.AddWithValue("vegetarian", eve.Vegetarian);
                command.Parameters.AddWithValue("non_vegetarian", eve.Non_Vegetarian);
                command.Parameters.AddWithValue("transport", eve.Transport);
                command.Parameters.AddWithValue("alerts", eve.Alerts);


                retVal = command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                // Log the exception
                ExceptionLogger.SendErrorToText(ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (connection != null && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return retVal;
        }
    }
}