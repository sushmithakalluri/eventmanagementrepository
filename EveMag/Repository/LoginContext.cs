﻿using EveMag.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace EveMag.Repository
{
    public class LoginContext
    {
        string connectionString = "";
        public int ValidateUser(Login login)
        {

            //string hostName = ConfigurationManager.AppSettings["DatabaseHostName"];
            //string dataBaseName = ConfigurationManager.AppSettings["DatabaseName"];
            connectionString = String.Format("Server={0};Port={1};Database={2};Uid={3};Pwd={4};",
            "localhost", "3306", "evemag", "root", "123456");

            int retVal = -1;
            string storedProcName = "ValidateUser";
            MySqlConnection connection = null;
            MySqlCommand command = null;
            try
            {

                connection = new MySqlConnection(connectionString);
                connection.Open();

                command = new MySqlCommand(storedProcName, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("UN", login.Username);
                command.Parameters.AddWithValue("PD", login.Password);

                object count = command.ExecuteScalar();
                retVal = Convert.ToInt16(count);
            }
            catch (Exception ex)
            {
                // Log the exception
                ExceptionLogger.SendErrorToText(ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (connection != null && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return retVal;
        }

    }
}