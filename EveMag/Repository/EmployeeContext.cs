﻿using EveMag.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace EveMag.Repository
{
    public class EmployeeContext : BaseDataContext
    {
        string connectionString = "";
        public EmployeeContext()
        {
            string hostName = ConfigurationManager.AppSettings["DatabaseHostName"];
            string dataBaseName = ConfigurationManager.AppSettings["DatabaseName"];
            connectionString = String.Format("Server={0};Port={1};Database={2};Uid={3};Pwd={4};",
            "localhost", "3306", "evemag", "root", "12345");
        }


        public int AddEmployee(Employee emp)
        {
            //ExceptionLogger.LogText("AddEvent Method--- values are");
            //ExceptionLogger.LogText("EventId :" + eve.EventId + ", " + eve.Description);
            int retVal = -1;
            string storedProcName = "AddEmployee";
            MySqlConnection connection = null;
            MySqlCommand command = null;
            try
            {
                connection = GetConnection();
                connection.Open();

                command = new MySqlCommand(storedProcName, connection);
                command.CommandType = CommandType.StoredProcedure;


                //command.Parameters.AddWithValue("EmpId", emp.EmpId);
                command.Parameters.AddWithValue("FirstName", emp.FirstName);
                command.Parameters.AddWithValue("LastName", emp.LastName);
                command.Parameters.AddWithValue("UserName", emp.UserName);
                command.Parameters.AddWithValue("Password1", emp.Password);
                command.Parameters.AddWithValue("PhoneNumber", emp.PhoneNumber);
                command.Parameters.AddWithValue("EmailId", emp.EmailId);
                command.Parameters.AddWithValue("IsActive", emp.IsActive);

                retVal = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Log the exception
                ExceptionLogger.SendErrorToText(ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (connection != null && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return retVal;
        }

        public Employee Get(int Id)
        {
            return GetEmployees().Where(x => x.EmpId == Id).First();
        }

        public List<Employee> GetEmployees()
        {

            MySqlCommand cmd = new MySqlCommand("GetEmployee", GetConnection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection.Open();
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);


            List<Employee> Emplist = new List<Employee>();
            while (dr.Read())
            {
                Employee Emp = new Employee();
                Emp.EmpId = Convert.ToInt32(dr[0]);
                Emp.FirstName = Convert.ToString(dr[1]);
                Emp.LastName = Convert.ToString(dr[2]);
                Emp.UserName = Convert.ToString(dr[3]);
                Emp.Password = Convert.ToString(dr[4]);
                Emp.PhoneNumber = Convert.ToString(dr[5]);
                Emp.EmailId = Convert.ToString(dr[6]);
                Emp.IsActive = Convert.ToBoolean(dr[7]);
                Emplist.Add(Emp);
            }
            dr.Close();
            return Emplist;
        }


        public int UpdateEmployee(Employee emp)
        {
            //ExceptionLogger.LogText("UpdateEvent Method--- values are");
            //ExceptionLogger.LogText("EventId :" + eve.EventId + ", " + eve.Description);
            int retVal = -1;
            string storedProcName = "UpdateEmployee";
            MySqlConnection connection = null;
            MySqlCommand command = null;
            try
            {
                connection = GetConnection();
                connection.Open();

                command = new MySqlCommand(storedProcName, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("EId", emp.EmpId);
                command.Parameters.AddWithValue("FirstName", emp.FirstName);
                command.Parameters.AddWithValue("LastName", emp.LastName);
                command.Parameters.AddWithValue("UserName", emp.UserName);
                command.Parameters.AddWithValue("Password1", emp.Password);
                command.Parameters.AddWithValue("PhoneNumber", emp.PhoneNumber);
                command.Parameters.AddWithValue("EmailId", emp.EmailId);
                command.Parameters.AddWithValue("IsActive", emp.IsActive);

                retVal = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Log the exception
                ExceptionLogger.SendErrorToText(ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (connection != null && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return retVal;
        }

        public int VerifyUserName(string Username)
        {
            connectionString = String.Format("Server={0};Port={1};Database={2};Uid={3};Pwd={4};",
           "localhost", "3306", "evemag", "root", "12345");

            int retVal = -1;
            string storedProcName = "VerifyUserName";
            MySqlConnection connection = null;
            MySqlCommand command = null;
            try
            {

                connection = GetConnection();
                connection.Open();

                command = new MySqlCommand(storedProcName, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("UN", Username);
              

                object count = command.ExecuteScalar();
                retVal = Convert.ToInt16(count);
            }
            catch (Exception ex)
            {
                // Log the exception
                ExceptionLogger.SendErrorToText(ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (connection != null && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return retVal;
        }

       
    }
   }
