﻿using EveMag.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace EveMag.Repository
{
    public class EventContext : BaseDataContext
    {

        public int AddEvent(Event eve)
        {
            //ExceptionLogger.LogText("AddEvent Method--- values are");
            //ExceptionLogger.LogText("EventId :" + eve.EventId + ", " + eve.Description);
            int retVal = -1;
            string storedProcName = "AddEvent";
            MySqlConnection connection = null;
            MySqlCommand command = null;
            try
            {
                connection = GetConnection();
                connection.Open();

                command = new MySqlCommand(storedProcName, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("eventId", eve.EventId);
                command.Parameters.AddWithValue("Name", eve.Name);
                command.Parameters.AddWithValue("Date", eve.Date);
                command.Parameters.AddWithValue("Venue", eve.Venue);
                command.Parameters.AddWithValue("Description", eve.Description);
                command.Parameters.AddWithValue("SeekFeedback", eve.SeekFeedback);
                command.Parameters.AddWithValue("VolunteerReq", eve.VolunteerReq);
                command.Parameters.AddWithValue("IsPublished", eve.IsPublished);

                retVal = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Log the exception
                ExceptionLogger.SendErrorToText(ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (connection != null && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return retVal;
        }

        public Event Get(int Id)
        {
            return GetEvents().Where(x => x.EventId == Id).First();
        }

        public List<Event> GetEvents()
        {

            MySqlCommand cmd = new MySqlCommand("GetEvents", GetConnection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection.Open();
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);


            List<Event> Eventslist = new List<Event>();
            while (dr.Read())
            {
                Event Eve = new Event();
                Eve.EventId = Convert.ToInt32(dr[0]);
                Eve.Name = Convert.ToString(dr[1]);
                Eve.Date = Convert.ToDateTime(dr["date"]);
                Eve.Venue = Convert.ToString(dr[3]);
                Eve.Description = Convert.ToString(dr[4]);
                Eve.SeekFeedback = Convert.ToBoolean(dr[5]);
                Eve.VolunteerReq = Convert.ToBoolean(dr[6]);
                Eve.IsPublished = Convert.ToBoolean(dr[7]);
                Eventslist.Add(Eve);
            }
            dr.Close();
            return Eventslist;
        }


        public int UpdateEvent(Event eve)
        {
            //ExceptionLogger.LogText("UpdateEvent Method--- values are");
            //ExceptionLogger.LogText("EventId :" + eve.EventId + ", " + eve.Description);
            int retVal = -1;
            string storedProcName = "UpdateEvent";
            MySqlConnection connection = null;
            MySqlCommand command = null;
            try
            {
                connection = GetConnection();
                connection.Open();

                command = new MySqlCommand(storedProcName, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.AddWithValue("EID", eve.EventId);
                command.Parameters.AddWithValue("Name", eve.Name);
                command.Parameters.AddWithValue("eventDate", eve.Date);
                command.Parameters.AddWithValue("Venue", eve.Venue);
                command.Parameters.AddWithValue("Description", eve.Description);
                command.Parameters.AddWithValue("SeekFeedback", eve.SeekFeedback);
                command.Parameters.AddWithValue("VolunteerReq", eve.VolunteerReq);
                command.Parameters.AddWithValue("IsPublished", eve.IsPublished);

                retVal = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Log the exception
                ExceptionLogger.SendErrorToText(ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (connection != null && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return retVal;
        }

        public int DeleteEvent(int id)
        {

            int retVal = -1;
            string storedProcName = "DeleteEvent";
            MySqlConnection connection = null;
            MySqlCommand command = null;
            try
            {
                connection = GetConnection();
                connection.Open();
                command = new MySqlCommand(storedProcName, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("eId", id);
                retVal = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Log the exception
                ExceptionLogger.SendErrorToText(ex);
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (connection != null && connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            return retVal;

        }
    }
}