﻿using EveMag.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace EveMag.Repository
{
    public class DeactivateContext : BaseDataContext
    {


        public Employee Get(int Id)
        {
            return GetEmployees().Where(x => x.EmpId == Id).First();
        }

        public List<Employee> GetEmployees()
        {

            MySqlCommand cmd = new MySqlCommand("Deactivate_Emp", GetConnection());
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection.Open();
            MySqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);


            List<Employee> Emplist = new List<Employee>();
            while (dr.Read())
            {
                Employee Emp = new Employee();
                Emp.EmpId = Convert.ToInt32(dr[0]);
                Emp.FirstName = Convert.ToString(dr[1]);
                Emp.LastName = Convert.ToString(dr[2]);
                Emp.UserName = Convert.ToString(dr[3]);
                Emp.EmailId = Convert.ToString(dr[4]);
                Emp.PhoneNumber = Convert.ToString(dr[5]);
                Emp.Password = Convert.ToString(dr[6]);
                Emp.IsActive = Convert.ToBoolean(dr[7]);
                Emplist.Add(Emp);
            }
            dr.Close();
            return Emplist;
        }
    }
}