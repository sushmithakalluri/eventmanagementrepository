-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.45-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema evemag
--

CREATE DATABASE IF NOT EXISTS evemag;
USE evemag;

--
-- Definition of table `details`
--

DROP TABLE IF EXISTS `details`;
CREATE TABLE `details` (
  `empId` int(11) NOT NULL auto_increment,
  `userName` varchar(50) default NULL,
  `password` varchar(50) default NULL,
  `firstName` varchar(50) default NULL,
  `lastName` varchar(50) default NULL,
  `emailId` varchar(50) default NULL,
  `phoneNumber` varchar(15) default NULL,
  `isValid` tinyint(1) default NULL,
  PRIMARY KEY  (`empId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `details`
--

/*!40000 ALTER TABLE `details` DISABLE KEYS */;
INSERT INTO `details` (`empId`,`userName`,`password`,`firstName`,`lastName`,`emailId`,`phoneNumber`,`isValid`) VALUES 
 (1,'admin','admin','sruti','vegesna','hfuh@gmail.com','23323243',1),
 (2,'admin','kjlkj','sruti','vegesna','hfuh@gmail.com','23323243',1),
 (3,'emp1','emp1','sush','kalluri','sush@gmail.com','2345676666',0);
/*!40000 ALTER TABLE `details` ENABLE KEYS */;


--
-- Definition of table `empfb`
--

DROP TABLE IF EXISTS `empfb`;
CREATE TABLE `empfb` (
  `empUsername` varchar(50) default NULL,
  `eventId` int(11) default NULL,
  `fid` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empfb`
--

/*!40000 ALTER TABLE `empfb` DISABLE KEYS */;
INSERT INTO `empfb` (`empUsername`,`eventId`,`fid`) VALUES 
 ('sruthi',2,8);
/*!40000 ALTER TABLE `empfb` ENABLE KEYS */;


--
-- Definition of table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `empId` int(11) NOT NULL auto_increment,
  `firstName` varchar(50) default NULL,
  `lastName` varchar(50) default NULL,
  `userName` varchar(100) default NULL,
  `phoneNumber` varchar(15) default NULL,
  `emailId` varchar(50) default NULL,
  `password` varchar(20) default NULL,
  `isActive` tinyint(1) default NULL,
  PRIMARY KEY  (`empId`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` (`empId`,`firstName`,`lastName`,`userName`,`phoneNumber`,`emailId`,`password`,`isActive`) VALUES 
 (30,'admin','addfsdf','admin','43698054','trhbgf','admin',1),
 (31,'dsfs','sdfds','emp1','24543543','emp1@gmail.com','emp1',1),
 (32,'ytgujh','hgjfn','hgj','','hgj','',1),
 (34,'','','','','','',1),
 (35,'hyuj','','fujhy','','','',1),
 (36,'','','','','','',1),
 (37,'','','','','','',1),
 (38,'dsfsp','sdfdsp','emp3','24543543','emp3@gmail.com','emp3',0);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;


--
-- Definition of table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `EventId` int(11) NOT NULL auto_increment,
  `Name` varchar(50) default NULL,
  `Date` datetime default NULL,
  `Venue` varchar(100) default NULL,
  `Description` varchar(200) default NULL,
  `SeekFeedback` tinyint(1) default NULL,
  `VolunteerReq` tinyint(1) default NULL,
  `IsPublished` tinyint(1) default NULL,
  PRIMARY KEY  (`EventId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` (`EventId`,`Name`,`Date`,`Venue`,`Description`,`SeekFeedback`,`VolunteerReq`,`IsPublished`) VALUES 
 (2,'sushmitha','1994-02-01 21:02:04','bvrit','fest',1,0,0),
 (4,'rfg','1994-01-01 23:02:04','gfdh','gfgfh',1,0,0),
 (5,'sruthi','1994-01-01 23:02:04','fdg','fdg',1,0,0),
 (6,'fdgf','1994-01-01 23:02:04','fdg','fdg',1,0,0);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;


--
-- Definition of table `eventdetails`
--

DROP TABLE IF EXISTS `eventdetails`;
CREATE TABLE `eventdetails` (
  `EventDetailsId` int(11) NOT NULL default '0',
  `Adults` int(11) default NULL,
  `Kids` int(11) default NULL,
  `Vegetarian` int(11) default NULL,
  `Non_Vegetarian` int(11) default NULL,
  `Transport` tinyint(1) default NULL,
  `Alerts` tinyint(1) default NULL,
  PRIMARY KEY  (`EventDetailsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventdetails`
--

/*!40000 ALTER TABLE `eventdetails` DISABLE KEYS */;
INSERT INTO `eventdetails` (`EventDetailsId`,`Adults`,`Kids`,`Vegetarian`,`Non_Vegetarian`,`Transport`,`Alerts`) VALUES 
 (2,4,5,1,1,1,1),
 (3,5,6,1,0,0,0),
 (4,2,3,2,3,0,1),
 (5,33,2,3,5,1,0),
 (6,2,2,2,2,1,0),
 (7,5,4,3,2,1,0),
 (8,8,9,4,2,1,0),
 (9,3,9,2,0,1,0),
 (10,3,2,9,2,1,0);
/*!40000 ALTER TABLE `eventdetails` ENABLE KEYS */;


--
-- Definition of table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `fId` int(11) NOT NULL default '0',
  `rating` int(11) default NULL,
  `feedbackDesc` varchar(200) default NULL,
  PRIMARY KEY  (`fId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` (`fId`,`rating`,`feedbackDesc`) VALUES 
 (6,0,NULL),
 (8,4,'Good');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;


--
-- Definition of table `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `Fname` varchar(50) default NULL,
  `Mname` varchar(50) default NULL,
  `Lname` varchar(50) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`Fname`,`Mname`,`Lname`) VALUES 
 ('asdf','asfd','asdfas'),
 ('2','asfd2','asdfas2'),
 ('Sruthi','Lakshmi','Vegesna'),
 ('Sush ','Kalluri','mitha'),
 ('Sushmi','Kalluri','mitha'),
 ('Sushmi','Kalluri','mitha');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;


--
-- Definition of procedure `AddEmployee`
--

DROP PROCEDURE IF EXISTS `AddEmployee`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddEmployee`(IN FirstName varchar(50),
IN LastName varchar(50),
IN UserName varchar(100),
IN EmailId varchar(50),
IN PhoneNumber varchar(15),
IN Password1 varchar(20),
IN IsActive bool)
BEGIN
insert into Employee(firstName,lastName,userName,phoneNumber,emailId,password,isActive) values (FirstName,LastName,UserName,PhoneNumber,EmailId,Password1,IsActive);
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `AddEmployee2`
--

DROP PROCEDURE IF EXISTS `AddEmployee2`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddEmployee2`(IN FirstName varchar(50),
IN LastName varchar(50),
IN UserName varchar(100),
IN EmailId varchar(50),
IN PhoneNumber varchar(15),
IN Password1 varchar(20),
IN IsActive bool)
BEGIN
insert into Employee(firstName,lastName,userName,phoneNumber,emailId,password,isActive) values (FirstName,LastName,UserName,PhoneNumber,EmailId,Password1,IsActive);
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `AddEvent`
--

DROP PROCEDURE IF EXISTS `AddEvent`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddEvent`(IN EventId int,
IN Name varchar(50),
IN Date DateTime,
IN Venue varchar(100),
IN Description varchar(20),
IN SeekFeedback bool,
IN VolunteerReq bool,
IN IsPublished bool)
BEGIN

insert into Event(eventId,name,date,venue,description,seekFeedback,volunteerReq,isPublished) values (EventId,Name,Date,Venue,Description,SeekFeedback,VolunteerReq,IsPublished);

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `AddEventDetails`
--

DROP PROCEDURE IF EXISTS `AddEventDetails`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddEventDetails`(IN EventDetailsId int,
IN Adults int,
IN Kids int,
IN Vegetarian int,
IN Non_Vegetarian int,
IN Transport bool,
IN Alerts bool)
BEGIN
insert into EventDetails(eventdetailsId,adults,kids,vegetarian,non_vegetarian,transport,alerts) values (EventDetailsId,Adults,Kids,Vegetarian,Non_Vegetarian,Transport,Alerts);
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `AddFeedback`
--

DROP PROCEDURE IF EXISTS `AddFeedback`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddFeedback`(IN FId int,
IN Rating int,
IN FeedbackDesc varchar(200),
IN EmpUsername varchar(50),
IN EventId int)
BEGIN

insert into Feedback(fId,rating,feedbackDesc) values (FId,Rating,FeedbackDesc);

insert into empfb(empUsername,eventId,fId) values (EmpUsername,EventId,FId);

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `Deactivate_Emp`
--

DROP PROCEDURE IF EXISTS `Deactivate_Emp`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Deactivate_Emp`()
BEGIN

select * from employee where isActive = 0;




END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `DeleteEvent`
--

DROP PROCEDURE IF EXISTS `DeleteEvent`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DeleteEvent`(IN eId int)
BEGIN
Delete from Event where EventId= eId;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `EditEventDetails`
--

DROP PROCEDURE IF EXISTS `EditEventDetails`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `EditEventDetails`(IN edId int,
IN adults int,
IN kids int,
IN vegetarian int,
IN non_vegetarian int,
IN transport bool,
IN alerts bool)
BEGIN

update eventdetails set EventDetailsId = edId,
Adults = adults,
Kids = kids,
Vegetarian = vegetarian,
Non_Vegetarian = non_vegetarian,
Transport = transport,
Alerts = alerts
where EventDetailsId = edId;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `Employee`
--

DROP PROCEDURE IF EXISTS `Employee`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Employee`(IN id INT)
begin select count(*) as total from employee where id = 1;
end $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `GetEmployee`
--

DROP PROCEDURE IF EXISTS `GetEmployee`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetEmployee`()
BEGIN

select * from Employee;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `GetEmployees`
--

DROP PROCEDURE IF EXISTS `GetEmployees`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetEmployees`()
BEGIN

select * from Employee;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `GetEventDetails`
--

DROP PROCEDURE IF EXISTS `GetEventDetails`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetEventDetails`()
BEGIN
select * from eventdetails;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `GetEvents`
--

DROP PROCEDURE IF EXISTS `GetEvents`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetEvents`()
BEGIN

select * from event;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `GetFeedbacks`
--

DROP PROCEDURE IF EXISTS `GetFeedbacks`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetFeedbacks`()
BEGIN

select * from feedback;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `InsertStudent`
--

DROP PROCEDURE IF EXISTS `InsertStudent`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertStudent`(IN fname Varchar (50),
IN Mname varchar (50),
IN Lname Varchar (50))
BEGIN

insert into student(fname, mname, lname) values (fname,Mname, Lname);
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `UpdateEmployee`
--

DROP PROCEDURE IF EXISTS `UpdateEmployee`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateEmployee`(IN EId int,
IN FirstName varchar(50),
IN LastName varchar(50),
IN UserName varchar(100),
IN PhoneNumber varchar(15),
IN EmailId varchar(50),
IN Password1 varchar(20),
IN IsActive bool)
BEGIN

update Employee set firstName=FirstName,
lastName=LastName,
userName=UserName,
phoneNumber=PhoneNumber,
emailId=EmailId,
password=Password1,
isActive=IsActive
where empId = EId;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `UpdateEvent`
--

DROP PROCEDURE IF EXISTS `UpdateEvent`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateEvent`(IN EID int,
IN Name varchar(50),
IN eventDate DateTime,
IN Venue varchar(100),
IN Description varchar(20),
IN SeekFeedback bool,
IN VolunteerReq bool,
IN IsPublished bool)
BEGIN

update Event set name=Name,
date = eventDate,
venue = Venue,
description = Description,
seekFeedback = SeekFeedback,
volunteerReq = VolunteerReq,
isPublished = IsPublished
where eventId = EID;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `ValidateUser`
--

DROP PROCEDURE IF EXISTS `ValidateUser`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ValidateUser`(IN UN varchar(50),
IN PD varchar(50))
BEGIN

select count(*) from details where username = UN and password = PD;



END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `VerifyUserName`
--

DROP PROCEDURE IF EXISTS `VerifyUserName`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `VerifyUserName`(IN UN varchar(50))
BEGIN

select count(*) from employee where username = UN;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
