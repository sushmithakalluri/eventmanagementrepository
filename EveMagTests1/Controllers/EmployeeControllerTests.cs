﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EveMag.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Cryptography;
namespace EveMag.Controllers.Tests
{
    [TestClass()]
    public class EmployeeControllerTests
    {
        [TestMethod()]
        public void EncryptPasswordTest()
        {
            
            string hashPwd = EncryptPassword("narender2");
            Assert.IsTrue(hashPwd.Length > 0);
        }
        [TestMethod()]
        public void EncryptPassword_CompareTest()
        {

            string hashPwd = EncryptPassword("narender2");
            string hashPwd2 = EncryptPassword("narender2");
            Assert.IsTrue(string.Compare(hashPwd,hashPwd2)==0);
        }
        public string EncryptPassword(string password)
        {
            HashAlgorithm hash = new SHA256Managed();
            byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(password);
            byte[] hashBytes = hash.ComputeHash(plainTextBytes);

            string hashValue = Convert.ToBase64String(hashBytes);
            return hashValue;
        }
    }
}
