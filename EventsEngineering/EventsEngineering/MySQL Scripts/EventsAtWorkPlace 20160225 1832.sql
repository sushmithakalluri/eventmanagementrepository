-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.45-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema eventsatworkplace
--

CREATE DATABASE IF NOT EXISTS eventsatworkplace;
USE eventsatworkplace;

--
-- Definition of table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `empId` int(11) NOT NULL auto_increment,
  `firstName` varchar(50) default NULL,
  `lastName` varchar(50) default NULL,
  `phoneNumber` varchar(15) default NULL,
  `emailAddress` varchar(150) default NULL,
  `isActive` tinyint(1) default NULL,
  PRIMARY KEY  (`empId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` (`empId`,`firstName`,`lastName`,`phoneNumber`,`emailAddress`,`isActive`) VALUES 
 (1,'sruthi','v','7893474747','sruthi@gmail.com',1),
 (2,'sushmitha','kalluri','6577687645','sush@gmail.com',1),
 (3,'priyanka','c','96030258585','priyanka@gmail.com',1),
 (4,'Ravali','karumuri','9675468675','ravali@gmail.com',1),
 (5,'sunitha','kanumuri','5783978365','sunitha@gmail.com',0);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;


--
-- Definition of table `employeeeventdetails`
--

DROP TABLE IF EXISTS `employeeeventdetails`;
CREATE TABLE `employeeeventdetails` (
  `empEventDetailsId` int(11) NOT NULL auto_increment,
  `eventId` int(11) NOT NULL,
  `empId` int(11) NOT NULL,
  `adultsCount` int(11) default NULL,
  `toddlersCount` int(11) default NULL,
  `boysOrGirlsCount` int(11) default NULL,
  `vegetariansCount` int(11) default NULL,
  `nonVegetariansCount` int(11) default NULL,
  `alcoholicsCount` int(11) default NULL,
  `transportReqd` tinyint(1) default NULL,
  `alertsReqd` tinyint(1) default NULL,
  `createdDate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`empEventDetailsId`,`eventId`,`empId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employeeeventdetails`
--

/*!40000 ALTER TABLE `employeeeventdetails` DISABLE KEYS */;
INSERT INTO `employeeeventdetails` (`empEventDetailsId`,`eventId`,`empId`,`adultsCount`,`toddlersCount`,`boysOrGirlsCount`,`vegetariansCount`,`nonVegetariansCount`,`alcoholicsCount`,`transportReqd`,`alertsReqd`,`createdDate`) VALUES 
 (1,4,5,2,3,2,2,4,4,1,1,'2016-02-25 14:28:56'),
 (2,5,6,2,1,1,3,3,4,0,0,'2016-02-25 14:29:55');
/*!40000 ALTER TABLE `employeeeventdetails` ENABLE KEYS */;


--
-- Definition of table `eventdetails`
--

DROP TABLE IF EXISTS `eventdetails`;
CREATE TABLE `eventdetails` (
  `eventDetailsId` int(11) NOT NULL auto_increment,
  `eventId` int(11) NOT NULL,
  `adultsFieldShow` tinyint(1) default NULL,
  `toddlersFieldShow` tinyint(1) default NULL,
  `genderFieldShow` tinyint(1) default NULL,
  `foodFieldShow` tinyint(1) default NULL,
  `alcoholicsFieldShow` tinyint(1) default NULL,
  `transportReqdFieldShow` tinyint(1) default NULL,
  `alertsReqdFieldShow` tinyint(1) default NULL,
  PRIMARY KEY  (`eventDetailsId`),
  KEY `eventId` (`eventId`),
  CONSTRAINT `eventdetails_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `eventinfo` (`eventId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventdetails`
--

/*!40000 ALTER TABLE `eventdetails` DISABLE KEYS */;
INSERT INTO `eventdetails` (`eventDetailsId`,`eventId`,`adultsFieldShow`,`toddlersFieldShow`,`genderFieldShow`,`foodFieldShow`,`alcoholicsFieldShow`,`transportReqdFieldShow`,`alertsReqdFieldShow`) VALUES 
 (1,2,1,1,1,0,1,0,0),
 (6,7,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `eventdetails` ENABLE KEYS */;


--
-- Definition of table `eventinfo`
--

DROP TABLE IF EXISTS `eventinfo`;
CREATE TABLE `eventinfo` (
  `eventId` int(11) NOT NULL auto_increment,
  `eventName` varchar(50) default NULL,
  `eventDescription` varchar(200) default NULL,
  `eventDate` datetime default NULL,
  `venue` varchar(100) default NULL,
  `venueURL` varchar(100) default NULL,
  `venueImage` varchar(100) default NULL,
  `venueLocationImage` varchar(100) default NULL,
  `eventAdImage` varchar(100) default NULL,
  `eventAdTooltip` varchar(100) default NULL,
  `contactPerson` varchar(50) default NULL,
  `contactEmail` varchar(50) default NULL,
  `feedbackFieldShow` tinyint(1) default NULL,
  `volunteerReqdFieldShow` tinyint(1) default NULL,
  `isPublished` tinyint(1) default NULL,
  `publishedDate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`eventId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventinfo`
--

/*!40000 ALTER TABLE `eventinfo` DISABLE KEYS */;
INSERT INTO `eventinfo` (`eventId`,`eventName`,`eventDescription`,`eventDate`,`venue`,`venueURL`,`venueImage`,`venueLocationImage`,`eventAdImage`,`eventAdTooltip`,`contactPerson`,`contactEmail`,`feedbackFieldShow`,`volunteerReqdFieldShow`,`isPublished`,`publishedDate`) VALUES 
 (2,'College Fest','Conducted every year','2016-02-21 00:00:00','BVRIT ','www.bvrithyderabad.in','lol','fdg','fgdh','This is the logo','sruthi','sru@gmail.com',1,1,1,'2016-02-21 00:00:00'),
 (7,'WesternPearl','WesternPearl','2016-02-21 00:00:00','WesternPearl','WesternPearl','WesternPearl','WesternPearl','WesternPearl','WesternPearl','WesternPearl','WesternPearl',0,0,0,'2016-02-21 00:00:00');
/*!40000 ALTER TABLE `eventinfo` ENABLE KEYS */;


--
-- Definition of table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `feedbackId` int(11) NOT NULL auto_increment,
  `empEventDetailsId` int(11) default NULL,
  `eventRating` int(11) default NULL,
  `comments` varchar(500) default NULL,
  PRIMARY KEY  (`feedbackId`),
  KEY `empEventDetailsId` (`empEventDetailsId`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`empEventDetailsId`) REFERENCES `employeeeventdetails` (`empEventDetailsId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` (`feedbackId`,`empEventDetailsId`,`eventRating`,`comments`) VALUES 
 (1,1,5,'dsgdfshg');
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;


--
-- Definition of table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `empId` int(11) NOT NULL,
  `userName` varchar(100) default NULL,
  `userPassword` varchar(200) default NULL,
  `isPasswordModified` tinyint(1) default NULL,
  `modifiedDate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  KEY `empId` (`empId`),
  CONSTRAINT `login_ibfk_1` FOREIGN KEY (`empId`) REFERENCES `employee` (`empId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` (`empId`,`userName`,`userPassword`,`isPasswordModified`,`modifiedDate`) VALUES 
 (1,'admin','jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=',0,'2016-02-24 11:05:38'),
 (2,'emp1','1j1jCGGjx60fBqmq7LbYdTkIrox3gZXkeOlmIdBvb8w=',0,'2016-02-24 11:06:23'),
 (3,'emp2','ipdeWo/nVkhTFrz3bScOrgO4R3ehMPissSWrhOkmatI=',0,'2016-02-24 11:08:58'),
 (4,'emp3','Dp0uUvuX6VBROG/LV2wLyUgJ1yEPd75AToZ47KSWepY=',0,'2016-02-25 12:58:36'),
 (5,'emp4','hNYs4PlCd/0H2S996v3no+PAKbwpdbStap1K9lDw8Cc=',0,'2016-02-25 12:59:15');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;


--
-- Definition of procedure `DeActiveEmployees`
--

DROP PROCEDURE IF EXISTS `DeActiveEmployees`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DeActiveEmployees`()
BEGIN

select * from employee where isActive = false;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `DeleteEvent`
--

DROP PROCEDURE IF EXISTS `DeleteEvent`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DeleteEvent`(IN eveId int)
BEGIN
delete from eventdetails where eventId = eveId;
delete from eventInfo where eventId= eveId;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `insertEmployee`
--

DROP PROCEDURE IF EXISTS `insertEmployee`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertEmployee`(IN fName varchar(50),
IN lName varchar(50),
IN uName varchar(100),
IN uPassword varchar(200),
IN phNo varchar(15),
IN email varchar(150),
IN active bool,
IN pwdModified bool)
BEGIN

insert into employee(firstName,lastName,phoneNumber,emailAddress,isActive) values (fname,lname,phno,email,active);

insert into login(empId,userName,userPassword,isPasswordModified) values (LAST_INSERT_ID(),uName,uPassword,pwdModified);

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `InsertEvent`
--

DROP PROCEDURE IF EXISTS `InsertEvent`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertEvent`(IN eveName varchar(50),
IN eveDesc varchar(200),
IN eveDate datetime,
IN vplace varchar(100),
IN vURL varchar(100),
IN vImage varchar(100),
IN vLocationImage varchar(100),
IN image varchar(100),
IN imageToolTip varchar(100),
IN cPerson varchar(50),
IN cEmail varchar(50),
IN seekFeedback bool,
IN volunteerReq bool,
IN publish bool,
IN datePublished datetime,
IN adults bool,
IN toddlers bool,
IN gender bool,
IN food bool,
IN alcoholic bool,
IN transport bool,
IN alerts bool)
BEGIN

insert into eventinfo(eventName,eventDescription,eventDate,venue,venueURL,venueImage,venueLocationImage,eventAdImage,eventAdTooltip,contactPerson,contactEmail,feedBackFieldShow,volunteerReqdFieldShow,isPublished,publishedDate) values (eveName,eveDesc,evedate,vplace,vURL,vImage,vLocationImage,image,imageToolTip,cPerson,cEmail,seekFeedback,volunteerReq,publish,datePublished);

insert into eventdetails(eventId,adultsFieldShow,toddlersFieldShow,genderFieldShow,foodFieldShow,alcoholicsFieldShow,transportReqdFieldShow,alertsReqdFieldShow) values (LAST_INSERT_ID(),adults,toddlers,gender,food,alcoholic,transport,alerts);

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `InsertEventDetails`
--

DROP PROCEDURE IF EXISTS `InsertEventDetails`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertEventDetails`(IN eveId int,
IN eId int,
IN aCount int,
IN tCount int,
IN boysOrGirls int,
IN vegCount int,
IN nonVegCount int,
IN alcoholics int,
IN transport bool,
IN alerts bool)
BEGIN

insert into employeeeventdetails(eventId,empId,adultsCount,toddlersCount,boysOrGirlsCount,vegetariansCount,nonVegetariansCount,alcoholicsCount,transportReqd,alertsReqd) values (eveId,eId,aCount,tCount,boysOrGirls,vegCount,nonVegCount,alcoholics,transport,alerts);

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `InsertFeedback`
--

DROP PROCEDURE IF EXISTS `InsertFeedback`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertFeedback`(IN rating int,
IN fbComments varchar(500))
BEGIN

insert into feedback(eventRating,comments) values (rating,fbComments);

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RetrieveAllEmployees`
--

DROP PROCEDURE IF EXISTS `RetrieveAllEmployees`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RetrieveAllEmployees`()
BEGIN

select e.empId empId,firstName,lastName,phoneNumber,emailAddress,isActive,userName,userPassword,isPasswordModified from employee e, login l where e.empid=l.empid;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RetrieveAllEventDetails`
--

DROP PROCEDURE IF EXISTS `RetrieveAllEventDetails`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RetrieveAllEventDetails`()
BEGIN

select * from employeeEventDetails;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RetrieveAllEvents`
--

DROP PROCEDURE IF EXISTS `RetrieveAllEvents`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RetrieveAllEvents`()
BEGIN
select eInfo.eventId, eInfo.*, eDetails.*  from eventInfo eInfo, eventdetails eDetails where eInfo.eventId=eDetails.eventId;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RetrieveAllFeedback`
--

DROP PROCEDURE IF EXISTS `RetrieveAllFeedback`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RetrieveAllFeedback`()
BEGIN

select * from feedback;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RetrieveEmployee`
--

DROP PROCEDURE IF EXISTS `RetrieveEmployee`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RetrieveEmployee`(IN employeeId int)
BEGIN


select e.empId,e.firstName,e.lastName,e.phoneNumber,e.emailAddress,e.isActive,l.userName,l.userPassword,l.isPasswordModified from employee e,login l where e.empId = employeeId and l.empId = employeeId; 


END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RetrieveEvents`
--

DROP PROCEDURE IF EXISTS `RetrieveEvents`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RetrieveEvents`()
BEGIN
select * from eventInfo;
select * from eventdetails;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `RetrieveFeedback`
--

DROP PROCEDURE IF EXISTS `RetrieveFeedback`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `RetrieveFeedback`()
BEGIN

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `UpdateEmployee`
--

DROP PROCEDURE IF EXISTS `UpdateEmployee`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateEmployee`(IN eid int,
IN fName varchar(50),
IN lName varchar(50),
IN uName varchar(100),
IN uPassword varchar(200),
IN phNo varchar(15),
IN email varchar(150),
IN active bool,
IN pwdModified bool)
BEGIN

update employee set firstName=fname,
lastName=lname,
phoneNumber=phno,
emailAddress=email,
isActive=active
where empId = eid;

update login set userName=uname,
userPassword=uPassword,
isPasswordModified=pwdModified
where empId = eid;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `UpdateEvent`
--

DROP PROCEDURE IF EXISTS `UpdateEvent`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateEvent`(IN eveId int,
IN eveName varchar(50),
IN eveDesc varchar(200),
IN eveDate datetime,
IN vplace varchar(100),
IN vURL varchar(100),
IN vImage varchar(100),
IN vLocationImage varchar(100),
IN image varchar(100),
IN imageToolTip varchar(100),
IN cPerson varchar(50),
IN cEmail varchar(50),
IN seekFeedback bool,
IN volunteerReq bool,
IN publish bool,
IN datePublished datetime,
IN edId int,
IN adults bool,
IN toddlers bool,
IN gender bool,
IN food bool,
IN alcoholic bool,
IN transport bool,
IN alerts bool)
BEGIN

update eventInfo set
eventName =eveName ,
eventDescription =eveDesc,
eventDate = evedate,
venue = vplace,
venueURL = vURL,
venueImage=vImage,
venueLocationImage=vLocationImage,
eventAdImage=image,
eventAdTooltip=imageToolTip,
contactPerson = cPerson,
contactEmail=cEmail,
feedBackFieldShow=seekFeedback,
volunteerReqdFieldShow=volunteerReq,
isPublished=publish,
publishedDate=datePublished
where eventId = eveId;

update eventdetails set adultsFieldShow = adults,
toddlersFieldShow = toddlers,
genderFieldShow = gender,
foodFieldShow = food,
alcoholicsFieldShow = alcoholic,
transportReqdFieldShow = transport,
alertsReqdFieldShow = alerts
where eventDetailsId = edId;




END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `UpdateEventDetails`
--

DROP PROCEDURE IF EXISTS `UpdateEventDetails`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateEventDetails`(IN edId int,
IN eveId int,
IN eId int,
IN aCount int,
IN tCount int,
IN boysOrGirls int,
IN vegCount int,
IN nonVegCount int,
IN alcoholics int,
IN transport bool,
IN alerts bool)
BEGIN

update employeeEventDetails set eventId = eveId,
empId = eId,
adultsCount = aCount,
toddlersCount = tCount,
boysOrGirlsCount = boysOrGirls,
vegetariansCount = vegCount,
nonVegetariansCount = nonVegCount,
alcoholicsCount = alcoholics,
transportReqd = transport,
alertsReqd = alerts
where empeventDetailsId = edId;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `ValidateUser`
--

DROP PROCEDURE IF EXISTS `ValidateUser`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ValidateUser`(IN uName varchar(50),
IN uPassword varchar(200))
BEGIN

select count(*) from login where userName = uName and userPassword = uPassword;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of procedure `VerifyUserName`
--

DROP PROCEDURE IF EXISTS `VerifyUserName`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER' */ $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `VerifyUserName`(IN uName varchar(100))
BEGIN

select count(*) from login where userName = uName;

END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
