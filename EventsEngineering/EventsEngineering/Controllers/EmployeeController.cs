﻿using EventsEngineering.DTO;
using EventsEngineering.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventsEngineering.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeManager employee = new EmployeeManager();
        // GET: Employee
        public ActionResult Index()
        {

            List<Employee> employeeList = employee.RetrieveEmployees();
            return View(employeeList);
        }

        // GET: Employee/Details
        [HttpGet]
        public ActionResult Details(int Id)
        {
            var emp = employee.GetEmployee(Id);
            return View("~/Views/Employee/Details.cshtml", emp);
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(Employee emp)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    emp.IsPasswordModified = false;
                    employee.InsertEmployee(emp);
                    return RedirectToAction("Index");
                }
                catch
                {
                    return View();
                }
            }
            return View();
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            var emp = employee.Get(id);
            return View("Edit", emp);
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Employee emp)
        {
            try
            {
                //emp.UserPassword = Utility.EncryptPassword(emp.UserPassword);
                employee.UpdateEmployee(emp);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        public int ValidateUserName(string username)
        {

            return employee.VerifyUserName(username);

        }
    }
}
