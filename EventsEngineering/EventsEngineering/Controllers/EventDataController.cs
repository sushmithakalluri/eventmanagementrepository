﻿using EventsEngineering.DTO;
using EventsEngineering.Manager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventsEngineering.Controllers
{
    public class EventDataController : Controller
    {
        EventDataManager eventmanager = new EventDataManager();
        // GET: EventData
        public ActionResult Index()
        {
            List<EventData> eventList = eventmanager.RetrieveEvents();
            //EventData data = null;
            // data.Links = new Dictionary<string, string>();
            return View(eventList);
        }

        // GET: EventDAta/Details/5
        public ActionResult Details(int Id)
        {
            var eventdata = eventmanager.RetrieveEvent(Id);
            return View("~/Views/EventData/Details.cshtml", eventdata);
        }

        // GET: EventData/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EventData/Create
        [HttpPost]
        public ActionResult Create(EventData eve)
        {
            try
            {
                eve.VenueImage = "venue_" + eve.EventName + ".jpg";
                eve.VenueLocationImage = "location_" + eve.EventName + ".jpg";
                eve.EventImage = eve.EventName + ".jpg";
                int retVal = eventmanager.InsertEvent(eve);
                if (retVal == 1)
                {
                    //TODO: Upload the file to server

                    HttpPostedFileBase baseFile = Request.Files[0];
                    HttpPostedFileBase baseFile1 = Request.Files[1];
                    HttpPostedFileBase baseFile2 = Request.Files[2];
                    var path = Path.Combine(Server.MapPath(@"\images"), eve.VenueImage);
                    var path1 = Path.Combine(Server.MapPath(@"\images"), eve.VenueLocationImage);
                    var path2 = Path.Combine(Server.MapPath(@"\images"), eve.EventImage);
                    baseFile.SaveAs(path);
                    baseFile1.SaveAs(path1);
                    baseFile2.SaveAs(path2);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        // GET: EventData/Edit/5
        public ActionResult Edit(int id)
        {
            var eve = eventmanager.Get(id);
            //var path = Path.Combine(@"..\..\images", eve.VenueImage);
            //eve.VenueImage = path;
            //var path1 = Path.Combine(@"..\..\images", eve.VenueImage);
            //eve.VenueLocationImage = path;
            //var path2 = Path.Combine(@"..\..\images", eve.AddImage);
            //eve.AddImage = path;
            return View("Edit", eve);
        }

        // POST: EventData/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EventData eve)
        {
            try
            {

                eventmanager.UpdateEvent(eve);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EventData/Delete/5
        public ActionResult Delete(int id)
        {
            var eve = eventmanager.Get(id);
            return View("Delete", eve);
        }

        // POST: EventData/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                eventmanager.DeleteEvent(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
