﻿using EventsEngineering.DTO;
using EventsEngineering.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventsEngineering.Controllers
{
    public class HomeController : Controller
    {
        EventDataManager eventmanager = new EventDataManager();
        // GET: EventData
        public ActionResult Index()
        {
            List<EventData> eventList = eventmanager.RetrieveEvents();
            return View(eventList);
        }
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}