﻿using EventsEngineering.DTO;
using EventsEngineering.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventsEngineering.Controllers
{
    public class LoginController : Controller
    {
        LoginManager loginmanager = new LoginManager();
        EventDataManager eventmanager = new EventDataManager();

        // GET: Login
        public ActionResult Login()
        {
            ViewBag.Name = Session["Uname"];
           return View();
        }

        // GET: Login
        public ActionResult Details()
        {
            ViewBag.Name = Session["Uname"];
            List<EventData> eventList = eventmanager.RetrieveEvents();
            return View(eventList);
        }
        // GET: Login
        public ActionResult Index(Login login)
        {
            try
            {

                int retVal = loginmanager.ValidateUser(login);
                Session["Uname"] = login.UserName;
                if (retVal == 1)
                {
                    if (login.UserName == "admin")
                    {
                        Session["Is_Admin"] = login.UserName;
                        return RedirectToAction("Login");//AdminHomePage
                    }
                    else
                    {
                        return RedirectToAction("Details");//EmployeeHomePage
                    }

                }
                else
                {
                    ViewBag.message = "Please enter valid details ";
                }
                return View();
            }
            catch
            {
                return View();
            }
        }
    }
}