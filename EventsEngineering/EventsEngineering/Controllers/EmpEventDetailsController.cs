﻿using EventsEngineering.DTO;
using EventsEngineering.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventsEngineering.Controllers
{
    public class EmpEventDetailsController : Controller
    {
        EmpEventDetailsManager edmanager = new EmpEventDetailsManager();
        // GET: EmpEventDetails
        public ActionResult Index()
        {
            List<EmpEventDetails> empdetailsList = edmanager.RetrieveAllEmpEventDetails();
            return View(empdetailsList);
        }
        // GET: EmpEventDetails/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: EmpEventDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EmpEventDetails/Create
        [HttpPost]
        public ActionResult Create(EmpEventDetails eventdetails)
       {
            try
            {
                edmanager.InsertEmployeeEventDetails(eventdetails);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EmpEventDetails/Edit/5
        public ActionResult Edit(int id)
        {
            var eventdetails = edmanager.Get(id);
            return View("Edit", eventdetails);
        }

        // POST: EmpEventDetails/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EmpEventDetails eventdetails)
        {
            try
            {
                edmanager.UpdateEmployeeEventDetails(eventdetails);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: EmpEventDetails/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: EmpEventDetails/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
