﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EventsEngineering.Startup))]
namespace EventsEngineering
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
