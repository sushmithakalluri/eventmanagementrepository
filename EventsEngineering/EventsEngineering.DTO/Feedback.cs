﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DTO
{
    public class Feedback
    {
        public int FeedbackId { get; set; }

        public int EmpEventDetailsId { get; set; }
        
        public int EventRating { get; set; }

        public string Comments { get; set; }
    }
}
