﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DTO
{
    public class EmpEventDetails
    {
        [Key]

        public int EmpEventDetailsId { get; set; }
        public int EventId { get; set; }
        public int EmpId { get; set; }
        [Required]
        public int AdultsCount { get; set; }
        [Required]
        public int ToddlersCount { get; set; }
        [Required]
        public int boysOrGirlsCount { get; set; }
        [Required]
        public int VegetariansCount { get; set; }
        [Required]
        public int NonVegetariansCount { get; set; }
        [Required]
        public int AlcoholicsCount { get; set; }
        
        public bool TransportReq { get; set; }
        
        public bool AlertsReq { get; set; }
         

    }
}
