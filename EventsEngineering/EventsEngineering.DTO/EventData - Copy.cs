﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DTO
{
    public class EventData
    {
        [Key]

        public int EventId { get; set; }

        [Required(ErrorMessage = "*Event Name field is required")]
        //[Range(3, 50, ErrorMessage = "*First Name should contain 3 - 50 characters")]
        public string EventName { get; set; }

        [Required(ErrorMessage = "*Event Description field is required")]
        //[Range(3, 200, ErrorMessage = "*First Name should contain 3 - 200 characters")]
        public string EventDescription { get; set; }

        [Required(ErrorMessage = "*Event Date field is required")]
        public DateTime EventDate { get; set; }

        [Required(ErrorMessage = "*Venue field is required")]
        //[Range(3, 100, ErrorMessage = "*Venue should contain 3 - 100 characters")]
        public string Venue { get; set; }

        [Required(ErrorMessage = "*Venue URL field is required")]
        //[Range(3, 100, ErrorMessage = "*Venue URL field should contain 3 - 100 characters")]
        public string VenueURL { get; set; }

        //[Required(ErrorMessage = "*Venue Image field is required")]
        //[Range(3, 100, ErrorMessage = "*Venue Image field should contain 3 - 100 characters")]
        public string VenueImage { get; set; }

        //[Required(ErrorMessage = "*Venue Location Image field is required")]
        //[Range(3, 100, ErrorMessage = "*Venue Location Image field should contain 3 - 100 characters")]
        public string VenueLocationImage { get; set; }

        //[Required(ErrorMessage = "*Image field is required")]
        //[Range(3, 100, ErrorMessage = "*Image field should contain 3 - 100 characters")]
        public string AddImage { get; set; }

        [Required(ErrorMessage = "*Image Tooltip field is required")]
        //[Range(3, 100, ErrorMessage = "*Image Tooltip field should contain 3 - 100 characters")]
        public string AddImageTooltip { get; set; }

        [Required(ErrorMessage = "*Contact Person field is required")]
        public string ContactPerson { get; set; }

        [Required(ErrorMessage = "*Contact Email field is required")]
        public string ContactEmail { get; set; }

        [Required(ErrorMessage = "*Feedback field is required")]
        public bool FeedbackFieldShow { get; set; }

        [Required(ErrorMessage = "*Volunteer field is required")]
        public bool VolunteerReqFieldShow { get; set; }

        [Required(ErrorMessage = "*Published field is required")]
        public bool IsPublished { get; set; }

        [Required(ErrorMessage = "*Published Date field is required")]
        public DateTime PublishedDate { get; set; }
        
        public int EventDetailsId { get; set; }

        [Required(ErrorMessage = "*Adults field is required")]
        public bool AdultsFieldShow { get; set; }

        [Required(ErrorMessage = "*Toddlers field is required")]
        public bool ToddlersFieldShow { get; set; }

        [Required(ErrorMessage = "*Gender field is required")]
        public bool GenderFieldShow { get; set; }

        [Required(ErrorMessage = "*Food field is required")]
        public bool FoodFieldShow { get; set; }

        [Required(ErrorMessage = "*Alcoholics field is required")]
        public bool AlcoholicsFieldShow { get; set; }

        [Required(ErrorMessage = "*Transport field is required")]
        public bool TransportReqFieldShow { get; set; }

        [Required(ErrorMessage = "*Alerts field is required")]
        public bool AlertsReqFieldShow { get; set; }

        public Dictionary<string, string> Links { get; set; }

    }
}
