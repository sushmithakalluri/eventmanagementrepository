﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DTO
{
    /// <summary>
    ///  Employee Type enumeration
    /// </summary>
    /// 
    public enum EmployeeType
    {
        /// <summary>
        /// 
        /// </summary>
        /// 
        Active = 0,

        /// <summary>
        /// 
        /// </summary>
        /// 
        InActive,

        /// <summary>
        /// 
        /// </summary>
        /// 
        All
    }
}
