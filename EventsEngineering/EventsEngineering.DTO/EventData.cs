﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DTO
{
    public class EventData : EventDetails
    {
        [Key]
        public int EventId { get; set; }
        [Required]
        public string EventName { get; set; }
        [Required]
        public string EventDescription { get; set; }
        [Required]
        public DateTime EventDate { get; set; }
        [Required]
        public string Venue { get; set; }
        [Required]
        public string VenueURL { get; set; }
        [Required]
        public string VenueImage { get; set; }
        [Required]
        public string VenueLocationImage { get; set; }
        [Required]
        public string EventImage { get; set; }
        [Required]
        public string EventImageTooltip { get; set; }
        [Required]
        public string ContactPerson { get; set; }
        [Required]
        public string ContactEmailAddress { get; set; }
        
    }
    public class EventDetails
    {
        [Required]
        public bool FeedbackFieldShow { get; set; }
        [Required]
        public bool VolunteerReqFieldShow { get; set; }
        [Required]
        public bool IsPublished { get; set; }
        [Required]
        public DateTime PublishedDate { get; set; }

        public int EventDetailsId { get; set; }
        [Required]
        public bool AdultsFieldShow { get; set; }
        [Required]
        public bool ToddlersFieldShow { get; set; }
        [Required]
        public bool GenderFieldShow { get; set; }
        [Required]
        public bool FoodFieldShow { get; set; }
        [Required]
        public bool AlcoholicsFieldShow { get; set; }
        [Required]
        public bool TransportReqFieldShow { get; set; }
        [Required]
        public bool AlertsReqFieldShow { get; set; }

        public Dictionary<string, string> Links { get; set; }

    }
}
