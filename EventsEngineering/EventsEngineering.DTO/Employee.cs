﻿using System.ComponentModel.DataAnnotations;

namespace EventsEngineering.DTO
{
    public class Employee : Login
    {
        [Key]
        public int EmpId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string EmailAddress { get; set; }
        [Required]
        public bool IsActive { get; set; }
        

    }
}
