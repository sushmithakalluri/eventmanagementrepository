﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DTO
{
    public class Login
    {
       
       [Display(Name = "Username")]
        public string UserName { get; set; }
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string UserPassword { get; set; }

        public bool IsPasswordModified { get; set; }

        public DateTime ModifiedDate { get; set; }   

    }
}
