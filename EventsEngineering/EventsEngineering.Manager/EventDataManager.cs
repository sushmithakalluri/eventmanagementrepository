﻿using EventsEngineering.DAL;
using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Manager
{
    public class EventDataManager
    {
        EventDataContext eventContext = new EventDataContext();

        public int InsertEvent(EventData eve)
        {

            return eventContext.InsertEvent(eve);
        }

        public EventData Get(int Id)
        {
            return eventContext.Get(Id);
        }

        public EventData RetrieveEvent(int Id)
        {
            return eventContext.RetrieveEvent(Id);
        }

        public List<EventData> RetrieveEvents()
        {

            return eventContext.RetrieveEvents();

        }

        public int UpdateEvent(EventData eve)
        {
            return eventContext.UpdateEvent(eve);

        }

        public int DeleteEvent(int id)
        {
            return eventContext.DeleteEvent(id);
        }
        
    }
}
