﻿using EventsEngineering.DAL;
using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Manager
{
    
    public class EmpEventDetailsManager
    {
        EmpEventDetailsContext edContext = new EmpEventDetailsContext();

        public int InsertEmployeeEventDetails(EmpEventDetails eventdetails)
        {
            return edContext.InsertEmployeeEventDetails(eventdetails);
        }

        public EmpEventDetails Get(int Id)
        {
            return edContext.Get(Id);
        }

        public List<EmpEventDetails> RetrieveAllEmpEventDetails()
        {
            return edContext.RetrieveAllEmpEventDetails();
        }

        public int UpdateEmployeeEventDetails(EmpEventDetails eventdetails)
        {
            return edContext.UpdateEmployeeEventDetails(eventdetails);
        }


    }
}
