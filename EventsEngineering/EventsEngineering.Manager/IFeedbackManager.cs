﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Manager
{
    interface IFeedbackManager
    {
        int InsertFeedback(Feedback feedback);

        Feedback RetrieveFeedback(int Id);
        Feedback Get(int Id);
        List<Feedback> RetrieveAllFeedback();
    }
}
