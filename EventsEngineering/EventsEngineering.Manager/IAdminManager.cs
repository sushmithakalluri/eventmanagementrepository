﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Manager
{
    public interface IAdminManager
    {
        Employee GetEmployeeInformation(string userName);

        List<Employee> GetEmployees(EmployeeType empType);

        bool UpdateEmployee(Employee employee);

        bool DeActivateEmployee(Employee employee);

    }
}
