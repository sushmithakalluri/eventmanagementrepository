﻿using EventsEngineering.DAL;
using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Manager
{
    public class EmployeeManager
    {
        EmployeeDataContext employeeContext = new EmployeeDataContext();
        public int InsertEmployee(Employee emp)
        {
          
            return employeeContext.InsertEmployee(emp);
        }

        public Employee GetEmployee(int Id)
        {
            return employeeContext.GetEmployee(Id);
        }

        public Employee Get(int Id)
        {
            return employeeContext.Get(Id);
        }

        public List<Employee> RetrieveEmployees()
        {
            
            return employeeContext.RetrieveEmployees();

        }

        public int UpdateEmployee(Employee emp)
        {
            return employeeContext.UpdateEmployee(emp);

        }

        public int VerifyUserName(string Username)
        {

            return employeeContext.VerifyUserName(Username);
        }

    }
}
