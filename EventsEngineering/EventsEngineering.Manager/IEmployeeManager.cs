﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Manager
{
    public interface IEmployeeManager
    {
        int InsertEmployee(Employee emp);

        Employee Get(int Id);

        List<Employee> RetrieveEmployees();

        int UpdateEmployee(Employee emp);

        int VerifyUserName(string Username);

        Employee GetEmployee(int Id);

    }
}
