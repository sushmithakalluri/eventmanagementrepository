﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventsEngineering.DTO;
using EventsEngineering.DAL;
namespace EventsEngineering.Manager
{
    public class LoginManager : ILoginManager
    {
        ILoginDataContext loginContext = new LoginDataContext();
        public int ValidateUser(Login login)
        {
            
            return loginContext.ValidateUser(login);
        }
    }
}
