﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Manager
{
    public interface IEmpEventDetailsManager
    {
        int InsertEmployeeEventDetails(EmpEventDetails eventdetails);
        EmpEventDetails Get(int Id);
        List<EmpEventDetails> RetrieveAllEmpEventDetails();
        int UpdateEmployeeEventDetails(EmpEventDetails eventdetails);
    }
}
