﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Manager
{
    interface IEventDataManager
    {
        int InsertEvent(EventData eve);

        EventData Get(int Id);

        EventData RetrieveEvent();

        List<EventData> RetrieveEvents(int Id);

        int UpdateEvent(EventData eve);

        int DeleteEvent(int id);
    }
}
