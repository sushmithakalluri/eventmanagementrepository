﻿using EventsEngineering.DAL;
using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Manager
{
    public class FeedbackManager
    {
        FeedbackDataContext fContext = new FeedbackDataContext();

        public int InsertFeedback(Feedback feedback)
        {
            return fContext.InsertFeedback(feedback);
        }

        public Feedback RetrieveFeedback(int Id)
        {
            return fContext.RetrieveFeedback(Id);
        }

        public Feedback Get(int Id)
        {
            return fContext.RetrieveFeedbackInfo(Id);
        }

        public List<Feedback> RetrieveAllFeedback()
        {
            return fContext.RetrieveAllFeedbacks();
        }


    }
}
