﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Common
{
    public interface ILogger
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// 
        void LogError(string message);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// 
        void LogError(Exception ex);
    }
}
