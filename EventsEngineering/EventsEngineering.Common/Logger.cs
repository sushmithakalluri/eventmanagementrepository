﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.Common
{
    /// <summary>
    /// 
    /// </summary>
    /// 
    public static class Logger
    {
        private static StreamWriter writer;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// 
        public static void LogError(string errorMessage)
        {
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "log.txt";
            using (writer = new StreamWriter(filePath, true))
            {
                if (writer != null)
                {
                    if (errorMessage != null)
                    {
                        writer.WriteLine(errorMessage);

                    }
                    else
                    {
                        writer.WriteLine("No error message found");
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// 
        public static void LogError(Exception ex)
        {
           LogError(ex.ToString());
        }


    }
}
