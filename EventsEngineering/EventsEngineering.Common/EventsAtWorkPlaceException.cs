﻿using System;

namespace EventsEngineering.Common
{
    /// <summary>
    /// 
    /// </summary>
    /// 
    public class EventsAtWorkPlaceException : ApplicationException
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorMessage"></param>
        public EventsAtWorkPlaceException(string errorMessage)
            : base(errorMessage)
        {
            Logger.LogError(errorMessage);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="ex"></param>
        /// 
        public EventsAtWorkPlaceException(string errorMessage, Exception ex)
            : base(errorMessage, ex)
        {
            Logger.LogError(string.Format("{0}{1}", errorMessage, ex.ToString()));
        }
    }
}
