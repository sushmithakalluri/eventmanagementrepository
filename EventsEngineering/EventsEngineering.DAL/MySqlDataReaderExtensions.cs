﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public static class MySqlDataReaderExtensions
    {
        public static T GetFieldValue<T>(this MySqlDataReader reader, string columnName)
        {
            return reader.GetFieldValue<T>(reader.GetOrdinal(columnName));
        }
    }
    public static class MySqlCommandExtensions
    {
        public static MySqlParameter AddParamValue(this MySqlCommand command, string columnName, object value)
        {
            return command.Parameters.AddWithValue(columnName, value);
        }
    }
}
