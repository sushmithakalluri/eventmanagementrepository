﻿using EventsEngineering.Common;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public abstract class BaseDataContext
    {
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        protected MySqlConnection GetConnection()
        {
            try
            {
                return new MySqlConnection(ConnectionString);
            }
            catch (ArgumentException ex)
            {
                throw new EventsAtWorkPlaceException("Invalid arguments issues while opening MySqlConnection", ex);
            }
            catch (MySqlException ex)
            {
                throw new EventsAtWorkPlaceException("Issue with opening MySqlConnection", ex);
            }
        }
    }
}
