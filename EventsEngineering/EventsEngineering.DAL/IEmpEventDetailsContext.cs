﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public interface IEmpEventDetailsContext
    {
        int InsertEmployeeEventDetails(EmpEventDetails eventdetails);

        EmpEventDetails Get(int Id);

        List<EmpEventDetails> RetrieveAllEmpEventDetails();
        
        int UpdateEmployeeEventDetails(EmpEventDetails eventdetails);
    }
}
