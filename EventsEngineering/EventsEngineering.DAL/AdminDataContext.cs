﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public class AdminDataContext : BaseDataContext, IAdminDataContext
    {

        public List<DTO.Employee> GetEmployees(DTO.EmployeeType empType)
        {
            throw new NotImplementedException();
        }

        public DTO.Employee GetEmployee(string userName)
        {
            throw new NotImplementedException();
        }

        public bool UpdateEmployee(DTO.Employee employee)
        {
            throw new NotImplementedException();
        }

        public bool DeActivateEmployee(DTO.Employee employee)
        {
            throw new NotImplementedException();
        }
    }
}
