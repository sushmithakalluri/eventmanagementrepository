﻿using EventsEngineering.Common;
using EventsEngineering.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace EventsEngineering.DAL
{
    public class EmployeeDataContext : BaseDataContext
    {

        /// <summary>
        /// Inserts the employee.
        /// </summary>
        /// <param name="emp">The emp.</param>
        /// <returns></returns>
        /// <exception cref="EventsAtWorkPlaceException">InsertEmployee method failed.\n</exception>
        public int InsertEmployee(Employee emp)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.InsertEmployeeName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("fName", emp.FirstName);
                        command.Parameters.AddWithValue("lName", emp.LastName);
                        command.Parameters.AddWithValue("uName", emp.UserName);
                        command.Parameters.AddWithValue("uPassword", Utility.EncryptPassword(emp.UserPassword));
                        command.Parameters.AddWithValue("PhNo", emp.PhoneNumber);
                        command.Parameters.AddWithValue("email", emp.EmailAddress);
                        command.Parameters.AddWithValue("active", emp.IsActive);
                        command.Parameters.AddWithValue("pwdModified", emp.IsPasswordModified);

                        return command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("InsertEmployee method failed.\n", ex);
            }
        }

        public Employee Get(int Id)
        {
            return RetrieveEmployees().Where(x => x.EmpId == Id).First();
        }

        public Employee GetEmployee(int Id)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.RetrieveEmployee, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("employeeId", Id);
                        using (MySqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            Employee emp = new Employee();
                            while (dr.Read())
                            {
                                emp.EmpId = Convert.ToInt32(dr["empId"]);
                                emp.FirstName = Convert.ToString(dr["firstName"]);
                                emp.LastName = Convert.ToString(dr["lastName"]);
                                emp.UserName = Convert.ToString(dr["userName"]);
                                emp.UserPassword = Convert.ToString(dr["userPassword"]);
                                emp.PhoneNumber = Convert.ToString(dr["phoneNumber"]);
                                emp.EmailAddress = Convert.ToString(dr["emailAddress"]);
                                emp.IsActive = Convert.ToBoolean(dr["isActive"]);
                                emp.IsPasswordModified = Convert.ToBoolean(dr["isPasswordModified"]);
                            }
                            return emp;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("Retrieve Employee method failed.\n", ex);
            }
        }

        public List<Employee> RetrieveEmployees()
        {
            List<Employee> emplist = new List<Employee>();
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.RetrieveEmployeeName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        using (MySqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (dr.Read())
                            {
                                Employee Emp = new Employee()
                                {
                                    EmpId = dr.GetFieldValue<int>("empId"),
                                    FirstName = dr.GetFieldValue<string>("firstName"),
                                    LastName = dr.GetFieldValue<string>("lastName"),
                                    UserName = dr.GetFieldValue<string>("userName"),
                                    UserPassword = dr.GetFieldValue<string>("userPassword"),
                                    PhoneNumber = dr.GetFieldValue<string>("phoneNumber"),
                                    EmailAddress = dr.GetFieldValue<string>("emailAddress"),
                                    IsActive = dr.GetFieldValue<bool>("isActive"),
                                    IsPasswordModified = dr.GetFieldValue<bool>("isPasswordModified")
                                };
                                emplist.Add(Emp);
                            }
                            return emplist;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("RetrieveEmployees method failed.\n", ex);
            }

        }

        public List<Employee> RetrieveDeActiveEmployees()
        {
            List<Employee> emplist = new List<Employee>();
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.DeActivateEmployeeName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        using (MySqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (dr.Read())
                            {
                                Employee Emp = new Employee()
                                {
                                    EmpId = dr.GetFieldValue<int>("empId"),
                                    FirstName = dr.GetFieldValue<string>("firstName"),
                                    LastName = dr.GetFieldValue<string>("lastName"),
                                    UserName = dr.GetFieldValue<string>("userName"),
                                    UserPassword = dr.GetFieldValue<string>("userPassword"),
                                    PhoneNumber = dr.GetFieldValue<string>("phoneNumber"),
                                    EmailAddress = dr.GetFieldValue<string>("emailAddress"),
                                    IsActive = dr.GetFieldValue<bool>("isActive"),
                                    IsPasswordModified = dr.GetFieldValue<bool>("isPasswordModified")
                                };
                                emplist.Add(Emp);
                            }
                            return emplist;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("RetrieveEmployees method failed.\n", ex);
            }
        }

        public int UpdateEmployee(Employee emp)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.UpdateEmployeeName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("eid", emp.EmpId);
                        command.Parameters.AddWithValue("fName", emp.FirstName);
                        command.Parameters.AddWithValue("lName", emp.LastName);
                        command.Parameters.AddWithValue("uName", emp.UserName);
                        command.Parameters.AddWithValue("uPassword", emp.UserPassword);
                        command.Parameters.AddWithValue("phNo", emp.PhoneNumber);
                        command.Parameters.AddWithValue("email", emp.EmailAddress);
                        command.Parameters.AddWithValue("active", emp.IsActive);
                        command.Parameters.AddWithValue("pwdModified", emp.IsPasswordModified);

                        return command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("Validate method failed", ex);

            }
        }

        public int VerifyUserName(string userName)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.VerifyUserName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("uName", userName);
                        object count = command.ExecuteScalar();
                        return Convert.ToInt32(count);
                    }
                }
            }
            catch (Exception ex)
            {
                // Log the exception
                throw new EventsAtWorkPlaceException("Validate method failed", ex);
            }
        }
    }
}
