﻿using EventsEngineering.Common;
using EventsEngineering.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public class LoginDataContext : BaseDataContext, ILoginDataContext
    {
        public int ValidateUser(DTO.Login login)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.ValidateUserName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                       
                            command.Parameters.AddWithValue("uName", login.UserName);
                            command.Parameters.AddWithValue("uPassword", Utility.EncryptPassword(login.UserPassword));

                            object count = command.ExecuteScalar();
                            return  Convert.ToInt16(count);
                        
                    }
                }
            }
            catch (MySqlException e)
            {
                throw new EventsAtWorkPlaceException("ValidateUser method failed", e);
            }
           
        }

        public Employee GetProfileInfo(string userName)
        {
            throw new NotImplementedException();
        }
        
    }
}

