﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public interface IEmployeeDataContext
    {
        int InsertEmployee(Employee emp);

        Employee Get(int Id);
        
        List<Employee> RetrieveEmployees();

        int UpdateEmployee(Employee emp);

        Employee GetEmployee(int Id);

        int VerifyUserName(string Username);
    }
}
