﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    /// <summary>
    /// 
    /// </summary>
    public interface IFeedbackDataContext
    {
        int InsertFeedback(Feedback feedback);

        Feedback RetrieveFeedback(int Id);

        Feedback RetrieveFeedbackInfo(int Id);

        List<Feedback> RetrieveAllFeedbacks();
    }
}
