﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public interface IEventDataContext
    {
        int InsertEvent(EventData eve);

        EventData Get(int Id);

        EventData RetrieveEvent(int Id);

        List<EventData> RetrieveEvents();

        int UpdateEvent(EventData eve);

        int DeleteEvent(int id);
    }
}
