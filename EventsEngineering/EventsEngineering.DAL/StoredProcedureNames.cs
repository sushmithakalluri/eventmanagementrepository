﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public static class StoredProcedureNames
    {
        public static string InsertEmployeeName = "InsertEmployee";
        public static string RetrieveEmployeeName = "RetrieveAllEmployees";
        public static string RetrieveEmployee = "RetrieveEmployee";
        public static string UpdateEmployeeName = "UpdateEmployee"; 
        public static string DeActivateEmployeeName = "DeActivateEmployee";
        public static string ValidateUserName = "ValidateUser";
        public static string VerifyUserName = "VerifyUserName";
        public static string InsertEventName = "InsertEvent";
        public static string RetrieveEventName = "RetrieveAllEvents";
        public static string RetrieveEvent = "RetrieveEvent";
        public static string UpdateEventName = "UpdateEvent";
        public static string DeleteEventName = "DeleteEvent";
        public static string InsertEmpEventDetails = "InsertEventDetails";
        public static string UpdateEmpEventDetails = "UpdateEventDetails";
        public static string RetrieveEmpEventDetails = "RetrieveAllEventDetails";
        public static string InsertFeedbackName = "InsertFeedback";
        public static string RetrieveAllFeedbackName = "RetrieveAllFeedback";
        public static string RetrieveFeedbackName = "RetrieveFeedback";


    }
}
