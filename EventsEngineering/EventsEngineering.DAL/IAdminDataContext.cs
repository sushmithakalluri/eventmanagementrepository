﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public interface IAdminDataContext
    {
        List<Employee> GetEmployees(EmployeeType empType);

        Employee GetEmployee(string userName);

        bool UpdateEmployee(Employee employee);

        bool DeActivateEmployee(Employee employee);

    }
}
