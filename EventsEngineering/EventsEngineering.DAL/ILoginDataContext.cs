﻿using EventsEngineering.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public interface ILoginDataContext
    {
        int ValidateUser(Login login);

        Employee GetProfileInfo(string userName);
        
    }
}
