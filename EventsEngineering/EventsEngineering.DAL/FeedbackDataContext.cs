﻿using EventsEngineering.Common;
using EventsEngineering.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    /// <summary>
    /// 
    /// </summary>
    /// 
    public class FeedbackDataContext : BaseDataContext, IFeedbackDataContext
    {
     
        public int InsertFeedback(Feedback feedback)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.InsertFeedbackName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        
                        command.Parameters.AddWithValue("rating", feedback.EventRating);
                        command.Parameters.AddWithValue("fbcomments", feedback.Comments);

                        return  command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("Insert Feedback method failed.\n", ex);
            }
            
        }

        public Feedback RetrieveFeedback(int Id)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.RetrieveFeedbackName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("fdId", Id);
                        using (MySqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            Feedback feedback = new Feedback();
                            while (dr.Read())
                            {
                                
                                feedback.FeedbackId = Convert.ToInt32(dr["feedbackId"]);
                                feedback.EmpEventDetailsId = Convert.ToInt32(dr["empEventDetailsId"]);
                                feedback.EventRating = Convert.ToInt32(dr["eventRating"]);
                                feedback.Comments = Convert.ToString(dr["comments"]);
                            }
                            return feedback;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("Retrieve Feedback method failed.\n", ex);
            }
        }

        public Feedback RetrieveFeedbackInfo(int Id)
        {
            return RetrieveFeedbacks(Id).FirstOrDefault();
        }

        public List<Feedback> RetrieveAllFeedbacks()
        {
            return RetrieveFeedbacks(0);
        }

        private List<Feedback> RetrieveFeedbacks(int feedbackId)
        {
            List<Feedback> feedbacklist = new List<Feedback>();
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.RetrieveAllFeedbackName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("feedbackId", feedbackId);

                        using (MySqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            Feedback feedback = new Feedback();
                            while (dr.Read())
                            {
                                feedback.FeedbackId = Convert.ToInt32(dr[0]);
                                feedback.EmpEventDetailsId = Convert.ToInt32(dr[1]);
                                feedback.EventRating = Convert.ToInt32(dr[2]);
                                feedback.Comments = Convert.ToString(dr[3]);
                                feedbacklist.Add(feedback);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("Retrieve Feedback method failed.\n", ex);
            }
          
            return feedbacklist;
        }
    }
}
