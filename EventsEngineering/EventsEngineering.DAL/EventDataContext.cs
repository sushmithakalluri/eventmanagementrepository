﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlTypes;
using System.Text;
using System.Threading.Tasks;
using EventsEngineering.DTO;
using EventsEngineering.Common;

namespace EventsEngineering.DAL
{
    public class EventDataContext : BaseDataContext
    {
        public int InsertEvent(EventData eve)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.InsertEventName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        //command.AddParamValue("eveName", eve.EventName);
                        command.Parameters.AddWithValue("eveName", eve.EventName);
                        command.Parameters.AddWithValue("eveDesc", eve.EventDescription);
                        command.Parameters.AddWithValue("eveDate", eve.EventDate);
                        command.Parameters.AddWithValue("vplace", eve.Venue);
                        command.Parameters.AddWithValue("vURL", eve.VenueURL);
                        command.Parameters.AddWithValue("vImage", eve.VenueImage);
                        command.Parameters.AddWithValue("vLocationImage", eve.VenueLocationImage);
                        command.Parameters.AddWithValue("image", eve.EventImage);
                        command.Parameters.AddWithValue("imageTooltip", eve.EventImageTooltip);
                        command.Parameters.AddWithValue("cPerson", eve.ContactPerson);
                        command.Parameters.AddWithValue("cEmail", eve.ContactEmailAddress);
                        command.Parameters.AddWithValue("seekFeedback", eve.FeedbackFieldShow);
                        command.Parameters.AddWithValue("volunteerReq", eve.VolunteerReqFieldShow);
                        command.Parameters.AddWithValue("publish", eve.IsPublished);
                        command.Parameters.AddWithValue("datePublished", eve.PublishedDate);
                        command.Parameters.AddWithValue("adults", eve.AdultsFieldShow);
                        command.Parameters.AddWithValue("toddlers", eve.ToddlersFieldShow);
                        command.Parameters.AddWithValue("gender", eve.GenderFieldShow);
                        command.Parameters.AddWithValue("food", eve.FoodFieldShow);
                        command.Parameters.AddWithValue("alcoholic", eve.AlcoholicsFieldShow);
                        command.Parameters.AddWithValue("transport", eve.TransportReqFieldShow);
                        command.Parameters.AddWithValue("alerts", eve.AlertsReqFieldShow);


                        return command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("InsertEmployee method failed.\n", ex);
            }
        }

        public EventData RetrieveEvent(int Id)
        {

            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.RetrieveEvent, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("eId", Id);
                        using (MySqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            EventData Eve = new EventData();
                            while (dr.Read())
                            {
                                Eve.EventId = Convert.ToInt32(dr["eventId"]);
                                Eve.EventName = Convert.ToString(dr["eventName"]);
                                Eve.EventDescription = Convert.ToString(dr["eventDescription"]);
                                Eve.EventDate = Convert.ToDateTime(dr["eventDate"]);
                                Eve.Venue = Convert.ToString(dr["venue"]);
                                Eve.VenueURL = Convert.ToString(dr["venueURL"]);
                                Eve.VenueImage = Convert.ToString(dr["venueImage"]);
                                Eve.VenueLocationImage = Convert.ToString(dr["venueLocationImage"]);
                                Eve.EventImage = Convert.ToString(dr["eventAdImage"]);
                                Eve.EventImageTooltip = Convert.ToString(dr["eventAdTooltip"]);
                                Eve.ContactPerson = Convert.ToString(dr["contactPerson"]);
                                Eve.ContactEmailAddress = Convert.ToString(dr["contactEmail"]);
                                Eve.FeedbackFieldShow = Convert.ToBoolean(dr["feedbackFieldShow"]);
                                Eve.VolunteerReqFieldShow = Convert.ToBoolean(dr["volunteerReqdFieldShow"]);
                                Eve.IsPublished = Convert.ToBoolean(dr["isPublished"]);
                                Eve.PublishedDate = Convert.ToDateTime(dr["publishedDate"]);
                                Eve.EventDetailsId = Convert.ToInt32(dr["eventDetailsId"]);
                                Eve.AdultsFieldShow = Convert.ToBoolean(dr["adultsFieldShow"]);
                                Eve.ToddlersFieldShow = Convert.ToBoolean(dr["toddlersFieldShow"]);
                                Eve.GenderFieldShow = Convert.ToBoolean(dr["genderFieldShow"]);
                                Eve.FoodFieldShow = Convert.ToBoolean(dr["foodFieldShow"]);
                                Eve.AlcoholicsFieldShow = Convert.ToBoolean(dr["alcoholicsFieldShow"]);
                                Eve.TransportReqFieldShow = Convert.ToBoolean(dr["transportReqdFieldShow"]);
                                Eve.AlertsReqFieldShow = Convert.ToBoolean(dr["alertsReqdFieldShow"]);

                            }
                            return Eve;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("RetrieveEvent method failed.\n", ex);
            }
        }

        public EventData Get(int Id)
        {
            return RetrieveEvents().Where(x => x.EventId == Id).First();
        }

        public List<EventData> RetrieveEvents()
        {
            List<EventData> Eventslist = new List<EventData>();

            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.RetrieveEventName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        using (MySqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            while (dr.Read())
                            {
                                EventData Eve = new EventData();
                                Eve.EventId = Convert.ToInt32(dr["eventId"]);
                                Eve.EventName = Convert.ToString(dr["eventName"]);
                                Eve.EventDescription = Convert.ToString(dr["eventDescription"]);
                                Eve.EventDate = Convert.ToDateTime(dr["eventDate"]);
                                Eve.Venue = Convert.ToString(dr["venue"]);
                                Eve.VenueURL = Convert.ToString(dr["venueURL"]);
                                Eve.VenueImage = Convert.ToString(dr["venueImage"]);
                                Eve.VenueLocationImage = Convert.ToString(dr["venueLocationImage"]);
                                Eve.EventImage = Convert.ToString(dr["eventAdImage"]);
                                Eve.EventImageTooltip = Convert.ToString(dr["eventAdTooltip"]);
                                Eve.ContactPerson = Convert.ToString(dr["contactPerson"]);
                                Eve.ContactEmailAddress = Convert.ToString(dr["contactEmail"]);
                                Eve.FeedbackFieldShow = Convert.ToBoolean(dr["feedbackFieldShow"]);
                                Eve.VolunteerReqFieldShow = Convert.ToBoolean(dr["volunteerReqdFieldShow"]);
                                Eve.IsPublished = Convert.ToBoolean(dr["isPublished"]);
                                Eve.PublishedDate = Convert.ToDateTime(dr["publishedDate"]);
                                Eve.EventDetailsId = Convert.ToInt32(dr["eventDetailsId"]);
                                Eve.AdultsFieldShow = Convert.ToBoolean(dr["adultsFieldShow"]);
                                Eve.ToddlersFieldShow = Convert.ToBoolean(dr["toddlersFieldShow"]);
                                Eve.GenderFieldShow = Convert.ToBoolean(dr["genderFieldShow"]);
                                Eve.FoodFieldShow = Convert.ToBoolean(dr["foodFieldShow"]);
                                Eve.AlcoholicsFieldShow = Convert.ToBoolean(dr["alcoholicsFieldShow"]);
                                Eve.TransportReqFieldShow = Convert.ToBoolean(dr["transportReqdFieldShow"]);
                                Eve.AlertsReqFieldShow = Convert.ToBoolean(dr["alertsReqdFieldShow"]);
                                Eventslist.Add(Eve);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("RetrieveEmployees method failed.\n", ex);
            }
            return Eventslist;
        }

        public int UpdateEvent(EventData eve)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.UpdateEventName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("eveId", eve.EventId);
                        command.Parameters.AddWithValue("eveName", eve.EventName);
                        command.Parameters.AddWithValue("eveDesc", eve.EventDescription);
                        command.Parameters.AddWithValue("eveDate", eve.EventDate);
                        command.Parameters.AddWithValue("vplace", eve.Venue);
                        command.Parameters.AddWithValue("vURL", eve.VenueURL);
                        command.Parameters.AddWithValue("vImage", eve.VenueImage);
                        command.Parameters.AddWithValue("vLocationImage", eve.VenueLocationImage);
                        command.Parameters.AddWithValue("image", eve.EventImage);
                        command.Parameters.AddWithValue("imageTooltip", eve.EventImageTooltip);
                        command.Parameters.AddWithValue("cPerson", eve.ContactPerson);
                        command.Parameters.AddWithValue("cEmail", eve.ContactEmailAddress);
                        command.Parameters.AddWithValue("seekFeedback", eve.FeedbackFieldShow);
                        command.Parameters.AddWithValue("volunteerReq", eve.VolunteerReqFieldShow);
                        command.Parameters.AddWithValue("publish", eve.IsPublished);
                        command.Parameters.AddWithValue("datePublished", eve.PublishedDate);
                        command.Parameters.AddWithValue("edId", eve.EventDetailsId);
                        command.Parameters.AddWithValue("adults", eve.AdultsFieldShow);
                        command.Parameters.AddWithValue("toddlers", eve.ToddlersFieldShow);
                        command.Parameters.AddWithValue("gender", eve.GenderFieldShow);
                        command.Parameters.AddWithValue("food", eve.FoodFieldShow);
                        command.Parameters.AddWithValue("alcoholic", eve.AlcoholicsFieldShow);
                        command.Parameters.AddWithValue("transport", eve.TransportReqFieldShow);
                        command.Parameters.AddWithValue("alerts", eve.AlertsReqFieldShow);


                        return command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("UpdateEmployee method failed.\n", ex);
            }
        }

        public int DeleteEvent(int id)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.DeleteEventName, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("eveId", id);
                        return command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("DeleteEvent method failed.\n", ex);
            }

        }
    }
}
