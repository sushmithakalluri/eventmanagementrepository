﻿using EventsEngineering.Common;
using EventsEngineering.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsEngineering.DAL
{
    public class EmpEventDetailsContext : BaseDataContext
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventdetails"></param>
        /// <returns></returns>
        /// 
        public int InsertEmployeeEventDetails(EmpEventDetails eventdetails)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.InsertEmpEventDetails, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("eveId", eventdetails.EventId);
                        command.Parameters.AddWithValue("eId", eventdetails.EmpId);
                        command.Parameters.AddWithValue("aCount", eventdetails.AdultsCount);
                        command.Parameters.AddWithValue("tCount", eventdetails.ToddlersCount);
                        command.Parameters.AddWithValue("boysOrGirls", eventdetails.boysOrGirlsCount);
                        command.Parameters.AddWithValue("vegCount", eventdetails.VegetariansCount);
                        command.Parameters.AddWithValue("nonVegCount", eventdetails.NonVegetariansCount);
                        command.Parameters.AddWithValue("alcoholics", eventdetails.AlcoholicsCount);
                        command.Parameters.AddWithValue("transport", eventdetails.TransportReq);
                        command.Parameters.AddWithValue("alerts", eventdetails.AlertsReq);

                        object count = command.ExecuteScalar();
                        return Convert.ToInt32(count);
                    }
                }
            }
            catch (MySqlException e)
            {
                throw new EventsAtWorkPlaceException("Insert method failed", e);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        /// 
        public EmpEventDetails Get(int Id)
        {
            return RetrieveAllEmpEventDetails().Where(x => x.EmpEventDetailsId == Id).First();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        public List<EmpEventDetails> RetrieveAllEmpEventDetails()
        {
            List<EmpEventDetails> empEventslist = new List<EmpEventDetails>();
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.RetrieveEmpEventDetails, dbConn))
                    {

                        using (MySqlDataReader dr = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {

                            while (dr.Read())
                            {
                                EmpEventDetails empdetails = new EmpEventDetails();
                                empdetails.EmpEventDetailsId = Convert.ToInt32(dr[0]);
                                empdetails.EventId = Convert.ToInt32(dr[1]);
                                empdetails.EmpId = Convert.ToInt32(dr[2]);
                                empdetails.AdultsCount = Convert.ToInt32(dr[3]);
                                empdetails.ToddlersCount = Convert.ToInt32(dr[4]);
                                empdetails.boysOrGirlsCount = Convert.ToInt32(dr[5]);
                                empdetails.VegetariansCount = Convert.ToInt32(dr[6]);
                                empdetails.NonVegetariansCount = Convert.ToInt32(dr[7]);
                                empdetails.TransportReq = Convert.ToBoolean(dr[8]);
                                empdetails.AlertsReq = Convert.ToBoolean(dr[9]);

                                empEventslist.Add(empdetails);
                            }
                            dr.Close();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new EventsAtWorkPlaceException("RetrieveEmployees method failed.\n", ex);
            }
            //finally
            //{
            //    if (command != null)
            //    {
            //        command.Dispose();
            //    }
            //    if (connection != null && connection.State != ConnectionState.Closed)
            //    {
            //        connection.Close();
            //        connection.Dispose();
            //    }
            //}
            return empEventslist;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventdetails"></param>
        /// <returns></returns>
        /// 
        public int UpdateEmployeeEventDetails(EmpEventDetails eventdetails)
        {
            try
            {
                using (MySqlConnection dbConn = GetConnection())
                {
                    dbConn.Open();
                    using (MySqlCommand command = new MySqlCommand(StoredProcedureNames.UpdateEmpEventDetails, dbConn))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("edId", eventdetails.EmpEventDetailsId);
                        command.Parameters.AddWithValue("eveId", eventdetails.EventId);
                        command.Parameters.AddWithValue("eId", eventdetails.EmpId);
                        command.Parameters.AddWithValue("aCount", eventdetails.AdultsCount);
                        command.Parameters.AddWithValue("tCount", eventdetails.ToddlersCount);
                        command.Parameters.AddWithValue("boysOrGirls", eventdetails.boysOrGirlsCount);
                        command.Parameters.AddWithValue("vegCount", eventdetails.VegetariansCount);
                        command.Parameters.AddWithValue("nonVegCount", eventdetails.NonVegetariansCount);
                        command.Parameters.AddWithValue("alcoholics", eventdetails.AlcoholicsCount);
                        command.Parameters.AddWithValue("transport", eventdetails.TransportReq);
                        command.Parameters.AddWithValue("alerts", eventdetails.AlertsReq);

                        return command.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException e)
            {
                throw new EventsAtWorkPlaceException("Update method failed", e);
            }
        }

    }
}
